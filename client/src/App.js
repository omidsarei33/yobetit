import React from 'react';
import './App.css';
import RegistrationForm from './Components/RegistrationForm';
import QuestionOne from './Components/QuestionOne';
import QuestionTwo from './Components/QuestionTwo';
import QuestionThree from './Components/QuestionThree';
import QuestionFour from './Components/QuestionFour';
import LoginForm from './Components/LoginForm';
import MainMenu from './Components/MainMenu';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import banner from "./assets/yobetit.jpeg";

function App() {
  return (
    <Router>
    <div className="App">
      <h2>Yobetit Test</h2>
      <div style={{
        height: "100px",
        backgroundColor:"#92B2FE"
    
      }}>
        <div style={{ height: "60px", width: "60px", borderRadius: "50%", position: "absolute", top: "5%", left: "5%" }} >
          <img src={banner} alt="banner" style={{ height: "60px", width: "60px", borderRadius: "50%" }} />
          <h3 style={{ width: "100px", color: "white", fontSize: "initial", fontWeight: "500", fontStyle: "italic", fontFamily: "-webkit-body", marginLeft: "-10%" }}>Omid Sarei</h3>
        </div>
      </div>
        <Switch>
          <Route path="/" exact={true} component={MainMenu}/>
          <Route path="/question1" component={QuestionOne} />
          <Route path="/question2" component={QuestionTwo} />
          <Route path="/question3" component={QuestionThree} />
          <Route path="/question4" component={QuestionFour} />
          <Route path="/question5" component={RegistrationForm} />
          <Route path="/question6" component={LoginForm} />
        </Switch>
    </div>
      </Router>

  );
}

export default App;
