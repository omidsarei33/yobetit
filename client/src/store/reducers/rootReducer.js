import { SAVE_COUNTRY_NAME,
        LIST_ALL_COUNTRIES,
        CHECK_CODE_FROM_LIST,
        SAVE_USER_DETAILS,
        RESET_STATE,
        JWT_AUTH,
        VERIFY_AUTH_TOKEN,
        UNAUTH_USER} from '../actions/actionType';

const initialState = {
    countryName: {},
    countriesList: [],
    checkCodeList: [],
    userDetails:{},
    authToken:{},
    verifyToken:{},
    unAuthUser:{}
}


function rootReducer(state = initialState, action) {
    switch (action.type) {
        case SAVE_COUNTRY_NAME:
            console.log(action, "actioncheck")
            return { ...state,countryName: action.payload }
        case LIST_ALL_COUNTRIES:
            console.log(action, "actioncheck")
            return {  ...state,countriesList: action.payload }
        case CHECK_CODE_FROM_LIST:
            console.log(action, "actioncheck")
            return  {
                ...state,
                checkCodeList: [...state.checkCodeList,...action.payload]
            }
        case SAVE_USER_DETAILS:
            console.log(action, "actioncheck")
            return {  ...state,userDetails: action.payload }
        case RESET_STATE:
            return {...state,
                    countryName:{},
                    countriesList:[],
                    checkCodeList:[],
                    userDetails:{}
                }
        case JWT_AUTH:
                console.log(action,"checkActionDATA");
                let accessToken;
                Object.entries(action).map((item)=>{
                accessToken = item[1].accessToken;
            })
            return {...state,authToken:accessToken}
        case VERIFY_AUTH_TOKEN:
            return {...state,verifyToken:action.payload}
        case UNAUTH_USER:
            return {...state,unAuthUser:action.payload}
        default:
            return state;
    }
}

export default rootReducer;