import {SAVE_COUNTRY_NAME,
        LIST_ALL_COUNTRIES,
        CHECK_CODE_FROM_LIST,
        SAVE_USER_DETAILS,
        RESET_STATE,
        JWT_AUTH,
        VERIFY_AUTH_TOKEN,
        UNAUTH_USER } from '../actions/actionType';

export  const saveCountryname = (countryName)=> dispatch =>{
    return fetch(`api/question1/${countryName}`)
        .then(response => response.text())
        .then(data=> dispatch({type:SAVE_COUNTRY_NAME,payload:JSON.parse(data)}))
        .catch(error=>console.log(error,"error in q1"))
}

export const listAllCountries = (region)=> dispatch =>{
    return fetch(`https://restcountries.eu/rest/v2/region/${region}`)
    .then(response => response.text())
    .then(data => dispatch({type:LIST_ALL_COUNTRIES,payload:JSON.parse(data)}))
}

export const checkCodeFromList = (countryCode)=> dispatch=>{
    return fetch(`api/question2/${countryCode}`)
            .then(response => response.text())
            .then(data => dispatch({type:CHECK_CODE_FROM_LIST,payload:JSON.parse(data)}))
}

export const saveUserDetails = (user)=> dispatch=>{
    return fetch(`api/question5/${user.name}/${user.email}/${user.password}`)
            .then(response => response.text())
            .then(data => dispatch({type:SAVE_USER_DETAILS,payload:JSON.parse(data)}))
}
export const jwtAuth = (user)=> dispatch=>{
    const userINput = {username:user.email,password:user.password};
    return fetch('api/login',{
        method:"post",
        headers:{
            "Content-Type": "application/json",
            mode: 'cors'
        },
        body:JSON.stringify(userINput)
    })
    .then(response => response.text())
    .catch(err =>console.error(err))
    .then(data =>{
        dispatch({type:JWT_AUTH,payload:JSON.parse(data)})
    })
    .catch(err =>dispatch({type:UNAUTH_USER,payload:err}))
    
}

export const unAuthUser = ()=>dispatch=>{
    return dispatch({type:UNAUTH_USER,payload:""})
}

export const verifyAuthToken = (user,token)=>dispatch =>{
    return fetch('api/question6',{
        method:"post",
        headers:{
            "Content-Type": "application/json",
            mode: 'cors',
            "Authorization": `Bearer ${token}`,
        },
        body:JSON.stringify({user,token})
    })
    .then(response => response.text())
    .catch(err =>console.error(err))
    .then(data =>dispatch({type:VERIFY_AUTH_TOKEN,payloa:JSON.parse(data)}))
    .catch(err =>console.error(err))
}

export const resetState = ()=>(dispatch) =>{
    return dispatch({type:RESET_STATE,payload:""})
}






