import React from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import QuestionCard from '../Components/QuestionCard';
import Media from 'react-media';
const MainMenu = () => {

    const useStyles = makeStyles({
        mainStyle: {
            width: "100%",
            margin: "auto",
        },
        button: {
            marginTop: "20px",
            marginBottom: '2%',
            width: "30%",
            height: "45px",
            marginLeft: "20px",
            fontSize: "10px",
            backgroundColor: "#92B2FE",
            "&:hover": {
                backgroundColor: "#CDCFF5",
                color: "#3f51b5",
                border: "1px solid #3f51b5"
            },
            "&:active": {
                backgroundColor: "white",
                color: "#3f51b5",
                border: "1px solid #3f51b5"
            }
        },
        link: {
            textDecoration: "none",
        }
    });

    const classes = useStyles();



    return (
        <div className={classes.mainStyle}>
            <Media query="(max-width: 599px)" render={() =>
                (
                    <div style={{ width: "90%", justifyContent: "center" }}>
                        <QuestionCard text="Question 1" customWidth="100%" marginBottom="20px" backText="using the API provided, gets a unique country from a specific name given" questionLink="/question1">
                            <Link className={classes.link} to="/question1">
                                <Button className={classes.button} variant="contained" color="primary" >Question1</Button>
                            </Link>
                        </QuestionCard>
                        <QuestionCard text="Question 2" customWidth="100%" marginBottom="20px" questionLink="/question2" backText="Using the same API write a function that returns a list of countries where their name matches at least a part of one of these string.">
                            <Link className={classes.link} to="/question2">
                                <Button className={classes.button} variant="contained" color="primary" >Question2</Button>
                            </Link>
                        </QuestionCard>
                        <QuestionCard text="Question 3" customWidth="100%" marginBottom="20px" questionLink="/question3" marginTop="0px" backText="Using the same API, list all the countries and a field to filter the country by name in react.">
                            <Link className={classes.link} to="/question3">
                                <Button className={classes.button} variant="contained" color="primary" >Question3</Button>
                            </Link>
                        </QuestionCard>
                        <QuestionCard text="Question 4" customWidth="100%" marginBottom="20px" marginTop="0px" questionLink="/question4" backText="Creat a slot machine with conditions provided">
                            <Link className={classes.link} to="/question4">
                                <Button className={classes.button} variant="contained" color="primary" >Question4</Button>
                            </Link>
                        </QuestionCard>
                        <QuestionCard text="Question 5" marginTop="0px" marginBottom="20px" customWidth="100%" questionLink="/question5" backText="Create form with validation all fields (Name, Email, Password) should be required, send to the back end. The endpoint need to save the user somewhere, you can use in memory or any database (SQL or NoSQL).">
                            <Link className={classes.link} to="/question5">
                                <Button className={classes.button} variant="contained" color="primary" >Question5</Button>
                            </Link>
                        </QuestionCard>
                        <QuestionCard text="Question 6" marginTop="0px" marginBottom="20px" customWidth="100%" questionLink="/question6" backText="Add Authentication using JWT create form to login and save the user info on local storage.">
                            <Link className={classes.link} to="/question6">
                                <Button className={classes.button} variant="contained" color="primary" >Question6</Button>
                            </Link>
                        </QuestionCard>
                    </div>
                )}
            />
            <div>
                <Media query="(min-width: 600px)" render={() =>
                    (
                        <React.Fragment>
                            <div style={{ display: "flex", width: "100%", justifyContent: "center" }}>

                                <QuestionCard text="Question 1" customWidth="400px" backText="using the API provided, gets a unique country from a specific name given" questionLink="/question1">
                                    <Link className={classes.link} to="/question1">
                                        <Button className={classes.button} variant="contained" color="primary" >Question1</Button>
                                    </Link>
                                </QuestionCard>
                                <QuestionCard text="Question 2" questionLink="/question2" backText="Using the same API write a function that returns a list of countries where their name matches at least a part of one of these string.">
                                    <Link className={classes.link} to="/question2">
                                        <Button className={classes.button} variant="contained" color="primary" >Question2</Button>
                                    </Link>
                                </QuestionCard>
                            </div>
                            <div style={{ display: "flex", width: "100%", justifyContent: "center", marginTop: "20px" }}>
                                <QuestionCard text="Question 3" questionLink="/question3" marginTop="0px" backText="Using the same API, list all the countries and a field to filter the country by name in react.">
                                    <Link className={classes.link} to="/question3">
                                        <Button className={classes.button} variant="contained" color="primary" >Question3</Button>
                                    </Link>
                                </QuestionCard>
                                <QuestionCard text="Question 4" customWidth="400px" marginTop="0px" questionLink="/question4" backText="Creat a slot machine with conditions provided">
                                    <Link className={classes.link} to="/question4">
                                        <Button className={classes.button} variant="contained" color="primary" >Question4</Button>
                                    </Link>
                                </QuestionCard>
                            </div>
                            <div style={{ display: "flex", width: "100%", justifyContent: "center", marginTop: "20px" }}>
                                <QuestionCard text="Question 5" marginTop="0px" customWidth="650px" questionLink="/question5" backText="Create form with validation all fields (Name, Email, Password) should be required, send to the back end. The endpoint need to save the user somewhere, you can use in memory or any database (SQL or NoSQL).">
                                    <Link className={classes.link} to="/question5">
                                        <Button className={classes.button} variant="contained" color="primary" >Question5</Button>
                                    </Link>
                                </QuestionCard>
                            </div>
                            <div style={{ display: "flex", width: "100%", justifyContent: "center", marginTop: "20px" }}>
                                <QuestionCard text="Question 6" marginTop="0px" customWidth="650px" questionLink="/question6" backText="Add Authentication using JWT create form to login and save the user info on local storage.">
                                    <Link className={classes.link} to="/question6">
                                        <Button className={classes.button} variant="contained" color="primary" >Question6</Button>
                                    </Link>
                                </QuestionCard>
                            </div>
                        </React.Fragment>
                    )}
                />

            </div>
        </div>
    )

}

export default MainMenu;