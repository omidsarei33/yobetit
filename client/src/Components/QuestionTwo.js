import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../store/actions/action';
import { Button, TextField} from '@material-ui/core';
import { Formik, Form } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import Select from 'react-select';
import { Link } from 'react-router-dom';
import BackArrow from '../assets/backArrow.png';
import { BallBeat } from 'react-pure-loaders';

const QuestionTwo = () => {

    const dispatch = useDispatch();
    let checkCountryCode = useSelector(state => state.checkCodeList);
    const [countryArr, setCountryArr] = useState([]);
    const [showClearSectio, setShowClearSection] = useState(false);
    const [showSelectDiv, setShowSelectDiv] = useState(false);
    const [loadingBool, setLoadingBool] = useState(false);
    const Array_Without_DoubleQuotes = JSON.stringify(countryArr).toString().replace(/"/g, "");
    const useStyles = makeStyles({
        table: {
            width: "80%",
            margin: "auto",
        },
        inputfield: {
            marginTop: "20px",
            width: "50%"
        },
        searchfield: {
            marginTop: "20px",
            width: "80%",
            height: "55px"

        },
        clearSection: {
            backgroundColor: "#282c34",
            color: "white",
            width: "80%",
            margin: "auto",
            borderRadius: ' 20px',
            marginBottom: "20px"
        },
        backBtn: {
            textDecoration: "none",
        },
        backButton: {
            backgroundColor: "white",
            "&:hover": {
                backgroundColor: "#F695B8",

            }
        },
        backButtonText: {
            marginLeft: "10px",
            "&:hover": {
                color: "white"
            }
        },
        showSelectDiv:{
            display: showSelectDiv? "block":"none",
            width: '50%' 
        }

    });

    const customStyles = {
        control: base => ({
            ...base,
            height: 55,
            minHeight: 35
        })
    };
    const classes = useStyles();


    const handleSearch = (() => {
        countryArr && countryArr.map((item) => {
            dispatch(Actions.checkCodeFromList(item.toLocaleLowerCase()));
        })
        setShowSelectDiv(true);

    })

    const options = (() => {
        let arr = [];
        checkCountryCode && checkCountryCode.map((item) => {
            arr.push({
                value: item.name,
                label: item.name
            })
        })
        return arr;
    })

    const onClickHandler = ()=>{
        setTimeout(() => {
            setLoadingBool(false);
        }, 1000);
        setLoadingBool(true);
    }
    const ResetStateHandler = ()=>{
        dispatch(Actions.resetState());
    }

    return (
        <div style={{ height: "100vh" }}>
            <div style={{ display: "flex", flexBasis: "100%", justifyContent: "center", marginTop: "30px" }}>
                <Link className={classes.backBtn} to="/">
                    <Button onClick={ResetStateHandler} className={classes.backButton} variant="contained">
                        <img src={BackArrow} alt="back-arow" height="25px" width="25px" />
                        <p className={classes.backButtonText}>Back To Menu</p>
                    </Button>
                </Link>
            </div>
            <Formik
                initialValues={{ name: '' }}
                onSubmit={(values, { resetForm }, actions) => {
                    console.log(values, "CHECKTHEVALUES")
                    setCountryArr([...countryArr, values.name]);
                    setShowClearSection(true);
                    resetForm();
                    setShowSelectDiv(false);
                }}
            >
                {props => (
                    <Form onSubmit={props.handleSubmit}>

                        <div style={{ width: '80%', margin: "auto" }}>
                            <div style={{ height: "20px" }}>
                            </div>
                            <h4>Create an list of keywords to be able to search for the countries who thier name partially match the keywords</h4>
                            <TextField className={classes.inputfield} required name="name" label="Country Name" variant="outlined" onChange={props.handleChange} value={props.values.name} />
                            <Button onClick={onClickHandler} style={{ marginTop: "20px", marginBottom: '60px', width: "35%", height: "55px", marginLeft: "20px", backgroundColor: "#FD80A5" }} variant="contained" type='submit'>Add</Button>
                        </div>
                        <div>
                            <BallBeat
                                color={'#123abc'}
                                loading={loadingBool}
                            />
                            {showClearSectio  && Object.keys(countryArr).length !== 0 && !loadingBool?
                                (
                                    <React.Fragment>
                                        <div className={classes.clearSection}>
                                            <p style={{ height: "20px" }}>{countryArr && countryArr.length > 0 && Array_Without_DoubleQuotes}</p>
                                            <Button style={{ marginTop: "20px", width: "30%", marginBottom: '60px', height: "55px", marginLeft: "20px", backgroundColor: "#FD80A5" }} variant="contained" onClick={() => { setCountryArr("") }}>Clear</Button>
                                        </div>
                                        <div style={{ display: "flex", width: '85%', margin: "auto" }}>
                                            <div style={{ width: "50%" }}>
                                                <Button style={{ marginTop: "20px", width: "80%", marginBottom: '60px', height: "55px", marginLeft: "20px", backgroundColor: "#FD80A5" }} variant="contained" onClick={handleSearch}>Find Matched</Button>
                                            </div>
                                            <div className={classes.showSelectDiv} >
                                                <Select
                                                    className={classes.searchfield}
                                                    placeholder="Select Country"
                                                    options={options()}
                                                    styles={customStyles}
                                                />
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )
                                :
                                (null)
                            }


                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    )
}

export default QuestionTwo;