import React, { userState, useState } from "react";
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../store/actions/action';
import BackArrow from '../assets/backArrow.png';

const Dashboard = () => {

    const dispatch = useDispatch();
    const [canLogout, setCanLogout] = useState(false);
    // Reset Handler
    const ResetStateHandler = () => {
        dispatch(Actions.resetState());
    }
    const logoutHandler = () => {

    }

    const useStyles = makeStyles({
        backBtn: {
            textDecoration: "none",
        },
        backButton: {
            backgroundColor: "white",
            "&:hover": {
                backgroundColor: "#F695B8",

            }
        },
        backButtonText: {
            marginLeft: "10px",
            "&:hover": {
                color: "white",
            }
        },
        link:{
            textDecoration:"none"
        }
    })

    const classes = useStyles();


    return (
        <div style={{ width: "80%", margin: "auto" }}>
            <h4>Welcome To Your Dashboard</h4><hr />
            <h5>Subscribe to our newsletter for more information about our bonuses</h5>
            <Link className={classes.link} to="/">
                <Button variant="contained" color="primary">Logout</Button>
            </Link>
        </div>
    )
}

export default Dashboard;