import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import cherr from "../assets/cherry.png";
import banana from "../assets/banana.png";
import lemon from "../assets/lemon.png";
import apple from "../assets/apple.png";
import { Link } from 'react-router-dom';
import BackArrow from '../assets/backArrow.png';

const QuestionFour = () => {

    const [apiresponse4, setApiResponse4] = useState();
    const [credit, setCredit] = useState(20);
    const fetchQuestion4 = async () => {
        fetch(`api/question4/${credit}`)
            .then(response => response.text())
            .then(data => {
                setApiResponse4(JSON.parse(data))
                setCredit(parseInt(JSON.parse(data).balance));
            })
    }
    const firstreeltxt = apiresponse4 && apiresponse4.firstReelSpin;
    const secondreeltxt = apiresponse4 && apiresponse4.secondReelSpin;
    const thirdreeltxt = apiresponse4 && apiresponse4.thirdReelSpin;


    const reel = (reel) => {
        switch (reel) {
            case "cherry":
                return cherr
            case "banana":
                return banana
            case "lemon":
                return lemon
            case "apple":
                return apple
            default:
                break;
        }
    }

    const useStyles = makeStyles({
        backBtn: {
            textDecoration: "none",
        },
        backButton:{
            backgroundColor:"white",
            "&:hover":{
                backgroundColor:"#F695B8",

            }
        },
        backButtonText:{
            marginLeft:"10px",
            "&:hover":{
                color:"white",
            }
        },
    })

    const classes = useStyles();

    return (
        <div>
            <div style={{ width: "90%",margin:"auto"}}>
                <div style={{ display: "flex", flexBasis: "100%", justifyContent: "center", marginTop: "30px" }}>
                    <Link className={classes.backBtn} to="/">
                        <Button className={classes.backButton} variant="contained">
                            <img src={BackArrow} alt="back-arrow" height="25px" width="25px" />
                            <p className={classes.backButtonText}>Back To Menu</p>
                        </Button>
                    </Link>
                </div>
                <div style={{ backgroundColor: "#282c34", borderRadius: "10px", color: "white", width: "100%", margin: "auto", boxShadow: "0px 0px 23px 10px rgba(0,0,40,0.31)" }}>
                    <p style={{ marginTop: "30px", height: "30px" }}>{firstreeltxt === secondreeltxt && secondreeltxt === thirdreeltxt ? "WOW 3 IN A ROW" : null}</p>
                    <div style={{ width: "100%", margin: "auto", marginTop: "30px", marginBottom: "30px" }}>
                        <img style={{ borderRadius: "20px", marginRight: "20px" }} src={reel(firstreeltxt)} height="100px" width="70px" />
                        <img style={{ borderRadius: "20px", marginRight: "20px" }} src={reel(secondreeltxt)} height="100px" width="70px" />
                        <img style={{ borderRadius: "20px", marginRight: "20px" }} src={reel(thirdreeltxt)} height="100px" width="70px" />
                    </div>
                    <div>
                        <p style={{ height: "20px", marginTop: "30px", width: "100%", margin: "auto" }}>  {apiresponse4 && apiresponse4.amountWin > 0 ? `You Won ${apiresponse4 && apiresponse4.amountWin}` : null}</p>
                        <p style={{ marginTop: "30px", width: "100%", margin: "auto" }}>The Balance is :{apiresponse4 && apiresponse4.balance}</p>
                    </div>
                    <div style={{ marginBottom: "30px" }}>
                        <Button onClick={fetchQuestion4} style={{ marginTop: "30px", marginBottom: "20px",backgroundColor:"#F695B8" }} variant="contained">Spin</Button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default QuestionFour;