import React ,{useState} from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup'
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import * as Actions from '../store/actions/action';
import { Button, TextField } from '@material-ui/core';
import { Link } from 'react-router-dom';
import BackArrow from '../assets/backArrow.png';
import {BallBeat} from 'react-pure-loaders';

const RegistrationForm = () => {

    const dispatch = useDispatch();
    const userSaved = useSelector(state => state.userDetails);
    const [loadingBool, setLoadingBool] = useState(false);

    const RegistrationSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        email: Yup.string()
            .email('Invalid email')
            .required('Required'),
        password: Yup.string()
            .min(2, 'Too Short!')
            .max(20, 'Too Long!')
            .required('Required'),

    });

    const ResetStateHandler = ()=>{
        dispatch(Actions.resetState());
    }
    const onClickHandler = () => {
        setTimeout(() => {
            setLoadingBool(false);
        }, 1000);
        setLoadingBool(true);
    }


    const useStyles = makeStyles({
        backBtn: {
            textDecoration: "none",
        },
        backButton: {
            backgroundColor: "white",
            "&:hover": {
                backgroundColor: "#F695B8",

            }
        },
        backButtonText: {
            marginLeft: "10px",
            "&:hover": {
                color: "white",
            }
        },
        form: {
            // backgroundColor:"#2A2966",
            width: "70%",
            margin: "auto",
            borderRadius: "10px"
        },
        registrationTitle: {
            height: "45px",
            width: "80%",
            backgroundColor: "rgb(35, 32, 85)",
            color: "white",
            borderRadius: "5px",
            alignItems: "center",
            display: "flex",
            margin: "auto",
            justifyContent: "center"
        }
    })

    const classes = useStyles();

    return (
        <div>
            <div style={{ display: "flex", flexBasis: "100%", justifyContent: "center", marginTop: "30px" }}>
                <Link className={classes.backBtn} to="/">
                    <Button onClick={ResetStateHandler} className={classes.backButton} variant="contained">
                        <img src={BackArrow} alt="back-arrow" height="25px" width="25px" />
                        <p className={classes.backButtonText}>Back To Menu</p>
                    </Button>
                </Link>
            </div>

            <div style={{marginTop:"30px",height:"60px" }}>
                {!loadingBool && userSaved && userSaved.email ? `User ${userSaved && userSaved.email} Was Saved Successfully`:
                (<BallBeat
                        color={'#123abc'}
                        loading={loadingBool}
                    />)
                }
            </div>
            <Formik
                initialValues={{ name: '', email: "", password: "" }}
                validationSchema={RegistrationSchema}
                onSubmit={(values) => {
                    console.log(values, "CHECKTHEVALUES")
                    dispatch(Actions.saveUserDetails(values));
                }}
            >
                {props => (
                    <Form onSubmit={props.handleSubmit} className={classes.form}>
                        <h5 className={classes.registrationTitle}>Registration Form</h5>
                        <div>
                            <TextField className={classes.inputTest} required name="name" label="Name" onChange={props.handleChange} value={props.values.name} />
                            {props.errors.name && props.touched.name ? (<div style={{ color: 'red' }}>{props.errors.name}</div>) : null}
                        </div>
                        <div>
                            <TextField required name="email" label="email" onChange={props.handleChange} value={props.values.email} />
                            {props.errors.email && props.touched.email ? (<div style={{ color: 'red' }}>{props.errors.email}</div>) : null}
                        </div>
                        <div>
                            <TextField required name="password" label="password" onChange={props.handleChange} value={props.values.password} />
                            {props.errors.password && props.touched.password ? (<div style={{ color: 'red' }}>{props.errors.password}</div>) : null}
                        </div>
                        <Button onClick={onClickHandler} style={{ marginTop: "30px", marginBottom: '60px',backgroundColor:"#FD80A5"}} variant="contained"  type='submit'>Sign Up</Button>
                    </Form>
                )}
            </Formik>
        </div>
    )
}

export default RegistrationForm;