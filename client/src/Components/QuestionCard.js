import React from "react";
import { makeStyles } from "@material-ui/core";
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

const QuestionCard = ({ text, marginTop, backText, questionLink,customWidth,marginBottom }) => {

    const useStyle = makeStyles({
        button: {
            backgroundColor:"#F695B8",
            color:"white",
            "&:hover": {
                backgroundColor: "#CDCFF5",
                color: "white",
            },
            "&:active": {
                backgroundColor: "white",
                color: "#3f51b5",
            }
        },
        link: {
            textDecoration: "none",
        },
        contrainer: {
            position: "relative",
            width: customWidth || "200px",
            height: "200px",
            marginRight: "20px",
            marginLeft: "20px",
            marginTop: marginTop || "20px",
            marginBottom: marginBottom|| "0px"
        },
        thecard: {
            position: "absolute",
            width: "100%",
            height: "100%",
            borderRadius: "15px",
            transformStyle: "preserve-3d",
            transition: "all 0.5s ease",
            "&:hover": {
                transform: "rotateY(180deg)"
            }
        },
        thefront: {
            position: "absolute",
            width: "100%",
            height: "100%",
            borderRadius: "15px",
            backfaceVisibility: "hidden",
            background: "#CDCFF5",
            color: "#333"

        },
        theback: {
            position: "absolute",
            width: "100%",
            height: "100%",
            borderRadius: "15px",
            backfaceVisibility: "hidden",
            background: "#92B2FE",
            color: "#333",
            transform: "rotateY(180deg)"
        }
    })


    const classes = useStyle();

    return (
        <div className={classes.contrainer}>
            <div className={classes.thecard}>
                <div className={classes.thefront}><p style={{ overflow: "hidden" }}>{text}</p></div>
                <div className={classes.theback}>
                    <p style={{ overflow: "hidden" }}>{backText}</p>
                    <Link className={classes.link} to={questionLink}>
                        <Button className={classes.button} variant="contained" >{text}</Button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default QuestionCard;