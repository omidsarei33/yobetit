import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../store/actions/action';
import { Button, TextField } from '@material-ui/core';
import { Formik, Form } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import BackArrow from '../assets/backArrow.png';
import { BallBeat } from 'react-pure-loaders';


const QuestionOne = () => {

    const dispatch = useDispatch();
    const countryName = useSelector(state => state.countryName);
    const [loadingBool, setLoadingBool] = useState(false);
    const LoginSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required')
    });
    console.log(countryName, "countryNamein Component")

    function createData(name, region, subRegion, nationality) {
        return { name, region, subRegion, nationality };
    }

    const rows = [
        createData('Country Name', countryName && countryName.countryname),
        createData('Region', countryName && countryName.region),
        createData('Sub Region', countryName && countryName.subregion),
        createData('Nationality', countryName && countryName.nationalities),
    ];

    const onClickHandler = () => {
        setTimeout(() => {
            setLoadingBool(false);
        }, 1000);
        setLoadingBool(true);
    }
    const ResetStateHandler = ()=>{
        dispatch(Actions.resetState());
    }

    const useStyles = makeStyles({
        table: {
            width: "80%",
            margin: "auto",
        },
        inputfield: {
            marginTop: "20px",
            width: "50%"
        },
        imageBox: {
            height: "120px",
            backgroundColor: "#282c34",
        },
        flagStyle: {
            position: "relative",
            display: countryName && countryName.flagUrl ? "inline" : "none",
            width: "130px",
            height: "120px",
            border: '0px'
        },
        bg: {
            marginLeft: "20%",
            marginRight: "20%"
            // background: "rgb(146,178,254)",
            // background: "linear-gradient(128deg, rgba(146,178,254,1) 26%, rgba(174,127,251,1) 93%)"
        },
        backBtn: {
            textDecoration: "none",
        },
        backButton: {
            backgroundColor: "white",
            "&:hover": {
                backgroundColor: "#F695B8",

            }
        },
        backButtonText: {
            marginLeft: "10px",
            "&:hover": {
                color: "white"
            }
        }

    });

    const classes = useStyles();
    return (
        <div className={classes.bg}>
                <div style={{ display: "flex", flexBasis: "100%", justifyContent: "center", marginTop: "30px" }}>
                    <Link className={classes.backBtn} to="/">
                        <Button onClick={ResetStateHandler} className={classes.backButton} variant="contained">
                            <img src={BackArrow} alt="back-arrow" height="25px" width="25px" />
                            <p className={classes.backButtonText}>Back To Menu</p>
                        </Button>
                    </Link>
                </div>
                <Formik
                    initialValues={{ name: '' }}
                    validationSchema={LoginSchema}
                    onSubmit={(values) => {
                        dispatch(Actions.saveCountryname(values.name.toLocaleLowerCase()));
                    }}
                >
                    {props => (
                        <Form onSubmit={props.handleSubmit}>

                            <div style={{ width: '100%' }}>
                                <div style={{ height: "20px" }}>
                                    {props.errors.name && props.touched.name ? (<div style={{ color: 'red', textAlign: "left", marginLeft: "7%" }}>{props.errors.name}</div>) : null}
                                </div>
                                <TextField className={classes.inputfield} required name="name" label="Name" variant="outlined" onChange={props.handleChange} value={props.values.name} />
                                <Button onClick={onClickHandler} style={{ marginTop: "20px", marginBottom: '60px', width: "35%", height: "55px", marginLeft: "20px", backgroundColor: "#FD80A5" }} variant="contained" type='submit'>Submit</Button>
                            </div>
                        </Form>
                    )}
                </Formik>
                {Object.keys(countryName).length !== 0 && !loadingBool ?
                    (
                        <React.Fragment>
                            <h2>Country Detail</h2>
                            <div className={classes.imageBox}>
                                <img className={classes.flagStyle} alt="flag"  src={countryName && countryName.flagUrl} />
                            </div>
                            <TableContainer component={Paper}>
                                <Table className={classes.table} aria-label="simple table">
                                    <TableHead>
                                    </TableHead>
                                    <TableBody>
                                        {rows.map((row) => (
                                            <TableRow key={row.name}>
                                                <TableCell component="th" scope="row">
                                                    {row.name}
                                                </TableCell>
                                                <TableCell align="right">{row.region}</TableCell>
                                                <TableCell align="right">{row.subregion}</TableCell>
                                                <TableCell align="right">{row.nationalities}</TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </React.Fragment>
                    )
                    :
                    (<BallBeat
                        color={'#123abc'}
                        loading={loadingBool}
                    />
                    )}
        </div>
    )
}

export default QuestionOne;