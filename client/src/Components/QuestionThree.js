import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../store/actions/action';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Select from 'react-select';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import BackArrow from '../assets/backArrow.png';


const QuestionThree = () => {

    const dispatch = useDispatch();
    const countriesList = useSelector(state => state.countriesList);
    const countryName = useSelector(state => state.countryName);
    console.log(countriesList, "countrylist");

    function createData(name, region, subRegion, nationality) {
        return { name, region, subRegion, nationality };
    }

    const rows = [
        createData('Country Name', countryName && countryName.countryname),
        createData('Region', countryName && countryName.region),
        createData('Sub Region', countryName && countryName.subregion),
        createData('Nationality', countryName && countryName.nationalities),
    ];

    const options = [
        { value: "asia", label: "Asia" },
        { value: "europe", label: "Europe" },
        { value: "africa", label: "Africa" },
        { value: "oceania", label: "Oceania" },
        { value: "americas", label: "Americas" },
        { value: "polar", label: "Polar" },
    ]

    const countryListFormatedArray = (() => {
        let arr = [];
        countriesList && countriesList.map((item) => {
            arr.push({
                value: item.name,
                label: item.name
            })
        })
        return arr;
    })
    
    const [selectedValue, setSelectedValue] = useState(3);
    const [selectedValue2, setSelectedValue2] = useState(3);
    console.log(selectedValue, "selectedValue")
    console.log(selectedValue2, "selectedValue22")
    // handle onChange event of the dropdown
    const handleChange = e => {
        setSelectedValue(e.value);

        dispatch(Actions.listAllCountries(e.value))

    }
    const handleChange2 = e => {
        setSelectedValue2(e.value);
        dispatch(Actions.saveCountryname(e.value.toLocaleLowerCase()));

    }
    const ResetStateHandler = () => {
        dispatch(Actions.resetState());
    }

    const useStyles = makeStyles({
        table: {
            width: "80%",
            margin: "auto",
        },
        inputfield: {
            marginTop: "20px",
            width: "50%"
        },
        imageBox: {
            height: "120px",
            backgroundColor: "#282c34",
            marginTop: "25px",
            marginBottom: "25px"

        },
        flagStyle: {
            position: "relative",
            display: countryName && countryName.flagUrl ? "inline" : "none",
            width: "130px",
            height: "120px",
            border: '0px'
        },
        backBtn: {
            textDecoration: "none",
        },
        backButton: {
            backgroundColor: "white",
            "&:hover": {
                backgroundColor: "#F695B8",

            }
        },
        backButtonText: {
            marginLeft: "10px",
            "&:hover": {
                color: "white",
            }
        },
        hideShowDiv: {
            width: '100%',
            margin: "auto",
            display: selectedValue !== 3 ? "block" : "none"
        }
    });
    const classes = useStyles();
    return (
        <div>
            <div style={{ display: "flex", flexBasis: "100%", justifyContent: "center", marginTop: "30px" }}>
                <Link className={classes.backBtn} to="/">
                    <Button onClick={ResetStateHandler} className={classes.backButton} variant="contained">
                        <img src={BackArrow} alt="back-arrow" height="25px" width="25px" />
                        <p className={classes.backButtonText}>Back To Menu</p>
                    </Button>
                </Link>
            </div>
            <div style={{ display: "flex", textAlign: "-webkit-center" }}>
                <div style={{ width: '100%', margin: "auto" }}>
                    <Select
                        className={classes.inputfield}
                        placeholder="Select Region"
                        value={options.filter(obj => obj.value === selectedValue)}
                        options={options}
                        onChange={handleChange}
                    />
                </div>

            </div>
            <div style={{ display: "flex", textAlign: "-webkit-center", height: "60px", }}>
                <div className={classes.hideShowDiv}>
                    <Select
                        className={classes.inputfield}
                        placeholder="Select Country"
                        value={countryListFormatedArray().filter(obj => obj.value === selectedValue2)}
                        options={countryListFormatedArray()}
                        onChange={handleChange2}
                    />
                </div>
            </div>
            {selectedValue !== 3 && selectedValue2 !== 3 ?
                (
                    <React.Fragment>
                        <div className={classes.imageBox}>
                            <img className={classes.flagStyle} alt="flag" src={countryName && countryName.flagUrl} />
                        </div>
                        <TableContainer component={Paper}>
                            <Table className={classes.table} aria-label="simple table">
                                <TableHead>
                                </TableHead>
                                <TableBody>
                                    {rows.map((row) => (
                                        <TableRow key={row.name}>
                                            <TableCell component="th" scope="row">
                                                {row.name}
                                            </TableCell>
                                            <TableCell align="right">{row.region}</TableCell>
                                            <TableCell align="right">{row.subregion}</TableCell>
                                            <TableCell align="right">{row.nationalities}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </React.Fragment>
                ) :
                null
            }

        </div>
    )
}

export default QuestionThree;