import React from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup'
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import * as Actions from '../store/actions/action';
import { Button, TextField } from '@material-ui/core';
import { Link } from 'react-router-dom';
import BackArrow from '../assets/backArrow.png';

const LoginForm = () => {

    const dispatch = useDispatch();
    const authTokenData = useSelector(state => state.authToken);
    const LoginSchema = Yup.object().shape({

        email: Yup.string()
            .email('Invalid email')
            .required('Required'),
        password: Yup.string()
            .min(2, 'Too Short!')
            .max(20, 'Too Long!')
            .required('Required'),

    });

    const ResetStateHandler = () => {
        dispatch(Actions.resetState());
    }

    const useStyles = makeStyles({
        backBtn: {
            textDecoration: "none",
        },
        backButton: {
            backgroundColor: "white",
            "&:hover": {
                backgroundColor: "#F695B8",

            }
        },
        backButtonText: {
            marginLeft: "10px",
            "&:hover": {
                color: "white",
            }
        },
        form: {
            // backgroundColor:"#2A2966",
            width: "70%",
            margin: "auto",
            borderRadius: "10px"
        },
        registrationTitle: {
            height: "45px",
            width: "80%",
            backgroundColor: "rgb(35, 32, 85)",
            color: "white",
            borderRadius: "5px",
            alignItems: "center",
            display: "flex",
            margin: "auto",
            justifyContent: "center"
        }
    })

    const classes = useStyles();

    return (
        <div>
            <div style={{ display: "flex", flexBasis: "100%", justifyContent: "center", marginTop: "30px" }}>
                <Link className={classes.backBtn} to="/">
                    <Button onClick={ResetStateHandler} className={classes.backButton} variant="contained">
                        <img src={BackArrow} alt="back-arrow" height="25px" width="25px" />
                        <p className={classes.backButtonText}>Back To Menu</p>
                    </Button>
                </Link>
            </div>

            <div style={{ marginTop: "30px", height: "60px" }}>
                <div>
                    <p>Username: malta@malta.com</p>
                    <p>Password: malta29</p>

                </div>
            </div>
            <Formik
                initialValues={{ email: "", password: "" }}
                validationSchema={LoginSchema}
                onSubmit={(values, actions) => {
                    console.log(values, "CHECKTHEVALUES")
                    dispatch(Actions.jwtAuth(values));
                    // if(authTokenData){
                    //     dispatch(Actions.verifyAuthToken(values,authTokenData))
                    // }                    
                }}
            >
                {props => (
                    <Form onSubmit={props.handleSubmit} className={classes.form}>
                        <div style={{ height: "30px" }}>
                            {typeof (authTokenData) === "undefined" ? <div style={{ height: "40px", color: "red" }}>USER NOT AUTHORIZED</div> :
                                Object.keys(authTokenData).length !== 0 ? <div>WELCOME ADMIN</div> : null}
                        </div>

                        <h5 className={classes.registrationTitle}>Login Form</h5>
                        <div>
                            <TextField required name="email" label="email" onChange={props.handleChange} value={props.values.email} />
                            {props.errors.email && props.touched.email ? (<div style={{ color: 'red' }}>{props.errors.email}</div>) : null}
                        </div>
                        <div>
                            <TextField required name="password" label="password" onChange={props.handleChange} value={props.values.password} />
                            {props.errors.password && props.touched.password ? (<div style={{ color: 'red' }}>{props.errors.password}</div>) : null}
                        </div>
                        <Button  style={{ marginTop: "30px", marginBottom: '60px', backgroundColor: "#FD80A5" }} variant="contained" type='submit'>Login</Button>
                    </Form>
                )}
            </Formik>
        </div>
    )
}

export default LoginForm;