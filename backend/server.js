require("dotenv").config();
const express = require("express");
const rp = require('request-promise');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 5000;
const path = require('path');
const jwt = require("jsonwebtoken");
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors());

app.get('/api/question1/:countryName', (req, res) => {
    try {
        const countryName = req.params.countryName;
        url = `https://restcountries.eu/rest/v2/name/${countryName}?fullText=true`;
        options = {
            uri: url,
            headers: {
                "User-Agent": "YobetitTest",
                "Access-Control-Allow-Origin": "*"
            }
        }
        return new Promise((resolve, reject) => {
            rp(options)
                .then((response) => {
                    const result = JSON.parse(response);
                    result.map((item) => {
                        console.log(item, 'checktheFUCKING ITEM')
                        const output = {
                            countryname: item.name,
                            region: item.region,
                            subregion: item.subregion,
                            nationalities: item.demonym,
                            flagUrl: item.flag
                        }
                        res.send(output);
                        resolve(item.name);
                    })

                })
                .catch((errorMessage) => {
                    console.log(errorMessage);
                    reject(errorMessage);
                })
        })
    }
    catch (e) {
        console.log(e, "errorMessage");
    }
})
app.get('/api/question2/:countryCode', (req, res) => {
    try {
        let countryCode = req.params.countryCode;
        let countryCodeCheck;
        let countryListArray = [];

        url = `https://restcountries.eu/rest/v2/name/${countryCode}`;
        options = {
            uri: url,
            headers: {
                "User-Agent": "YobetitTest",
                "Access-Control-Allow-Origin": "*"
            }
        }
        return new Promise((resolve, reject) => {
            rp(options)
                .then((response) => {
                    const result = JSON.parse(response);
                    let output = [];
                    result && result.map((item) => {
                        output.push({
                            name: item.name
                        })
                    })
                    res.send(output);
                    resolve(output)
                })
                .catch((errorMessage) => {
                    console.log(errorMessage);
                    reject(errorMessage);
                })
        })
    }
    catch (e) {
        console.log(e, "errorMessage");
    }
})

app.get('/api/question4/:credit', (req, res) => {

    let coins = parseInt(req.params.credit)
    const Reel1 = ["cherry", "lemon", "apple", "lemon", "banana", "banana", "lemon", "lemon"];
    const Reel2 = ["lemon", "apple", "lemon", "lemon", "cherry", "apple", "banana", "lemon"];
    const Reel3 = ["lemon", "apple", "lemon", "lemon", "cherry", "apple", "banana", "lemon"];

    // generating random numbers for each reel
    rndNumber1 = Math.floor((Math.random() * 8));
    rndNumber2 = Math.floor((Math.random() * 8));
    rndNumber3 = Math.floor((Math.random() * 8));

    const firstReelSpin = Reel1[rndNumber1];
    const secondReelSpin = Reel2[rndNumber2];
    const thirdReelSpin = Reel3[rndNumber3];
    const result = `${firstReelSpin} ${secondReelSpin} ${thirdReelSpin}`
    let amountWin = 0;
    // CHECKING FOR 3 IN ROW
    try {
        if (coins >= 0) {
            if (firstReelSpin == "cherry" && firstReelSpin.includes(secondReelSpin) && secondReelSpin.includes(thirdReelSpin)) {
                coins += 50;
                amountWin = 50;
                console.log(coins, "HERE");
            }
            else if (firstReelSpin == "apple" && firstReelSpin.includes(secondReelSpin) && secondReelSpin.includes(thirdReelSpin)) {
                coins += 20;
                amountWin = 20;
                console.log(coins, "HERE");
            }
            else if (firstReelSpin == "banana" && firstReelSpin.includes(secondReelSpin) && secondReelSpin.includes(thirdReelSpin)) {
                coins += 20;
                amountWin = 20;
                console.log(coins, "HERE");
            }
            else if (firstReelSpin == "lemon" && firstReelSpin.includes(secondReelSpin) && secondReelSpin.includes(thirdReelSpin)) {
                coins += 3;
                amountWin = 3;
                console.log(coins, "HERE");
            }
            //CHECKING FOR 2 IN ROW
            else if (firstReelSpin == "cherry" && firstReelSpin.includes(secondReelSpin)) {
                coins += 40;
                amountWin = 40;
                console.log(coins, "HERE");
            }
            else if (firstReelSpin == "apple" && firstReelSpin.includes(secondReelSpin)) {
                coins += 10;
                amountWin = 10;
                console.log(coins, "HERE");
            }
            else if (firstReelSpin == "banana" && firstReelSpin.includes(secondReelSpin)) {
                coins += 5;
                amountWin = 5;
                console.log(coins, "HERE");
            }
            else {
                coins -= 1;
                console.log(coins, "CHECK THE BALANC");
            }
        }
        else {
            res.send("NOT ENOUGH MONEY");
        }

    }
    catch (e) {
        console.log(error)
    }
    const output = {
        balance: coins,
        spinResult: result,
        firstReelSpin,
        secondReelSpin,
        thirdReelSpin,
        amountWin
    }
    res.send(output)
})

app.get('/api/question5/:name/:email/:password', (req, res) => {
    const name = req.params.name;
    const email = req.params.email;
    const password = req.params.password;
    const userInput = { name, email, password }

    try {
        if (typeof localStorage === "undefined" || localStorage === null) {
            var LocalStorage = require('node-localstorage').LocalStorage;
            localStorage = new LocalStorage('./scratch');
        }
        localStorage.setItem('userDetails', JSON.stringify(userInput));
        const result = localStorage.getItem('userDetails');
        res.send(result);
    }
    catch (e) {
        console.log(e)
    }
})

app.post('/api/question6',authenticateToken, (req, res) => {
    const body = req && req.body;
    jwt.verify(body.token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) return res.sendStatus(403);
    })
    const userInput = { username: req.body.user.email, password: req.body.user.password }
    try {
        if (typeof localStorage === "undefined" || localStorage === null) {
            var LocalStorage = require('node-localstorage').LocalStorage;
            localStorage = new LocalStorage('./authUser');
        }
        localStorage.setItem('userDetails', JSON.stringify(userInput));
        const result = localStorage.getItem('userDetails');
        res.send(result);


    }
    catch (e) {
        console.log(e)
    }
})

app.post("/api/login", (req, res) => {
    console.log("IM HERE", req.body);
    const admin = "malta@malta.com";
    const adminPass = "malta29";
    const username = req.body.username;
    const pass = req.body.password;
    if (username === admin && pass === adminPass) {
        const user = { name: username, password: pass };
        const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
        res.set("Content-Type", "application/json")
        res.send({ accessToken: accessToken });
    } else {
        res.send({ unAuth: " Un Authorized User" });
        res.sendStatus(401);
    }

})

function authenticateToken(req, res, next) {
    const authHeader = req.headers["authorization"]
    const token = authHeader && authHeader.split(' ')[1]

    if (!token) return res.sendStatus(401);

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) return res.sendStatus(403);
        req.user = user
        next()
    })
}


if (process.env.NODE_ENV === 'production') {
    // Serve any static files
    app.use(express.static(path.join(__dirname, 'client/build')));

    // Handle React routing, return all requests to React app
    app.get('*', function (req, res) {
        res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
    });
}


app.listen(port, () => { console.log(`Server Started on Port ${port}`) });

